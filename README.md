# GNU/Linux dotfiles
[![Author](https://img.shields.io/badge/author-Kuroyasha512-blue.svg)](https://gitlab.com/Kuroyasha512)
[![Software License](https://img.shields.io/badge/license-GNU_GPLv3-brightgreen.svg)](https://gitlab.com/Kuroyasha512/dotfiles/blob/master/README.md#license)  
backup dotfile from my void linux

# Programs configs

* i3-wm
* i3status
* i3-gaps
* vim
* bash
* emacs

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE](https://gitlab.com/Kuroyasha512/keycode/blob/master/LICENSE) file for details

